# SiteMate - Yuliawan Putra

Project Name
This project for sitemate from Yuliawan Putra

Table of Contents
Overview
Prerequisites
Installation
Usage
Folder Structure
Docker Configuration
Contributing
License
Overview

This project is about CRUD for Movie (because it has data, title, and description).
The backend of the project is using Node.js with express and other libraries.
The frontend of the project is using Next.Js with material ui and other libraries.
The project was created as simple as possible with consideration of the time.

Prerequisites
List of prerequisites required to run the project:

Docker
npm
Node.js (if any)

Installation
Clone the repository:
bash
Copy code
git clone https://gitlab.com/yuliawanpp/sitemate
Navigate to the project directory: (webapp or webapi)
run 'npm install' on webapp and webapi

Usage
Instructions on how to use the project:

Start the Docker containers:
cmd
go to root directory
run docker-compose up
wait until both service running
Access the web app at http://localhost:3000.
Access the web API at http://localhost:3001/api/movies.

Folder Structure

project-directory/
│
├── webapp/ # Web application files
│ ├── Dockerfile # Dockerfile for web app
│ └── ...
│
├── webapi/ # Web API files
│ ├── Dockerfile # Dockerfile for web API
│ └── ...
│
└── docker-compose.yml # Docker Compose configuration file

Webapi Unit Test

- run 'npx jest' in webapi folder
