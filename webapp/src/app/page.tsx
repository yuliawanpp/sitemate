"use client";

import styles from "./page.module.css";
import { useEffect, useState } from "react";
import {
  Container,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
  TablePagination,
  CircularProgress,
  IconButton,
  Modal,
  Button,
} from "@mui/material";
import AddCircleOutlineIcon from "@mui/icons-material/AddCircleOutline";
import Link from "next/link";
import { Movie, getAllMovies } from "@/lib/api/movie";
import { Edit, Delete } from "@mui/icons-material";
import { deleteMovieById } from "@/lib/api/movie";

export default function Home() {
  const [movies, setMovies] = useState<Movie[]>([]);
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(5);
  const [loading, setLoading] = useState(true);
  const [totalMovies, setTotalMovies] = useState(0);
  const [isDeleteModalOpen, setIsDeleteModalOpen] = useState(false);
  const [selectedDeleteMovieId, setSelectedDeleteMovieId] = useState(-1);

  const openModal = (movieId: number) => {
    setSelectedDeleteMovieId(movieId);
    setIsDeleteModalOpen(true);
  };

  const closeModal = () => {
    setSelectedDeleteMovieId(-1);
    setIsDeleteModalOpen(false);
  };

  const getMovies = async () => {
    setLoading(true);
    try {
      const result = await getAllMovies(page + 1, rowsPerPage);
      setMovies(result.data);
      setTotalMovies(result.total);
    } catch (error) {
      console.error("Error fetching movies:", error);
    } finally {
      setLoading(false);
    }
  };

  useEffect(() => {
    getMovies();
  }, [page, rowsPerPage]);

  const handleChangePage = (event: unknown, newPage: number) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const handleConfirmDelete = async () => {
    if (selectedDeleteMovieId !== -1) {
      await deleteMovieById(selectedDeleteMovieId);
      getMovies();
      closeModal();
    }
  };

  return (
    <main className={styles.main}>
      <Container>
        {loading ? (
          <CircularProgress />
        ) : (
          <>
            <h1
              style={{
                display: "flex",
                justifyContent: "space-between",
                alignItems: "center",
              }}
            >
              Movies
              <Link href="/movies/new">
                Add
                <IconButton color="primary" aria-label="add movie">
                  <AddCircleOutlineIcon />
                </IconButton>
              </Link>
            </h1>
            <TableContainer component={Paper}>
              <Table>
                <TableHead>
                  <TableRow>
                    <TableCell>Number</TableCell>
                    <TableCell>Title</TableCell>
                    <TableCell>Description</TableCell>
                    <TableCell>Action</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {movies !== undefined &&
                    movies.map((movie) => (
                      <TableRow key={movie.Id}>
                        <TableCell>
                          <Link href={`/movies/${movie.Id}`}>
                            {movie.Id + 1}
                          </Link>
                        </TableCell>
                        <TableCell>{movie.Title}</TableCell>
                        <TableCell>{movie.Description}</TableCell>
                        <TableCell>
                          <div
                            style={{ display: "flex", alignItems: "center" }}
                          >
                            <Link href={`/movies/${movie.Id}`}>
                              <IconButton aria-label="edit">
                                <Edit />
                              </IconButton>
                            </Link>
                            <IconButton
                              aria-label="delete"
                              onClick={() => openModal(movie.Id)}
                            >
                              <Delete />
                            </IconButton>
                          </div>
                        </TableCell>
                      </TableRow>
                    ))}
                </TableBody>
              </Table>
              <TablePagination
                rowsPerPageOptions={[5, 10, 25]}
                component="div"
                count={totalMovies}
                rowsPerPage={rowsPerPage}
                page={page}
                onPageChange={handleChangePage}
                onRowsPerPageChange={handleChangeRowsPerPage}
              />
            </TableContainer>
          </>
        )}
      </Container>
      <Modal
        style={{
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
        }}
        open={isDeleteModalOpen}
        onClose={closeModal}
      >
        <div style={{ backgroundColor: "white", padding: 20 }}>
          <p>Are you sure you want to delete this movie?</p>
          <Button onClick={handleConfirmDelete}>Yes</Button>
          <Button onClick={closeModal}>No</Button>
        </div>
      </Modal>
    </main>
  );
}
