import React, { useState, useEffect } from "react";
import { useRouter } from "next/router";
import { getMovieById, updateMovieById } from "@/lib/api/movie";
import { TextField, Button, Typography, Container } from "@mui/material";

const UpdateMovie = () => {
  const router = useRouter();
  const { id } = router.query as { id: string }; // Get movie ID from URL parameters

  const [title, setTitle] = useState("");
  const [description, setDescription] = useState("");

  useEffect(() => {
    const getMovieData = async (id: number) => {
      try {
        const movieData = await getMovieById(id);
        setTitle(movieData.Title);
        setDescription(movieData.Description);
      } catch (error) {
        console.error("Error get By id movie:", error);
      }
    };

    if (id || id === "0") {
      getMovieData(parseInt(id));
    }
  }, [id]);

  const handleSubmit = async (e: React.FormEvent) => {
    e.preventDefault();
    await updateMovieById({
      Id: parseInt(id),
      Title: title,
      Description: description,
    });
  };

  return (
    <Container maxWidth="sm">
      <Typography variant="h4" gutterBottom>
        Edit Movie
      </Typography>
      <form onSubmit={handleSubmit}>
        <TextField
          label="Title"
          variant="outlined"
          fullWidth
          margin="normal"
          value={title}
          onChange={(e) => setTitle(e.target.value)}
        />
        <TextField
          label="Description"
          variant="outlined"
          fullWidth
          multiline
          rows={4}
          margin="normal"
          value={description}
          onChange={(e) => setDescription(e.target.value)}
        />
        <Button variant="contained" color="primary" type="submit">
          Save Changes
        </Button>
      </form>
    </Container>
  );
};

export default UpdateMovie;
