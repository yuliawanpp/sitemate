import axios from "axios";
import router from "next/router";

export interface Movie {
  Id: number;
  Title: string;
  Description: string;
}

export interface GetMoviesResponse {
  data: Movie[];
  total: number;
  totalPages: number;
  currentPage: number;
  pageSize: number;
}

const BASE_URL = "http://localhost:3001/api";
const MOVIE_BASE_URL = "/movies";

export const getAllMovies = async (
  page: number,
  pageSize: number
): Promise<GetMoviesResponse> => {
  const response = await axios.get(`${BASE_URL}${MOVIE_BASE_URL}`, {
    params: {
      page,
      pageSize,
    },
  });

  return response.data;
};

export const createMovie = async (title: string, description: string) =>{
  try {
    const response = await axios.post(`${BASE_URL}${MOVIE_BASE_URL}`, { title, description });
    if (response.status === 201) {
      // Redirect to the main page after adding the movie
      router.push('/');
    } else {
     console.log(`error create movie : ${response.data}`)
    }
  } catch (error) {
    console.error('Error adding movie:', error);
  }
}

export const getMovieById = async (id: number) => {
  try {
    const response = await axios.get(`${BASE_URL}${MOVIE_BASE_URL}/${id}`);
    if (response.status === 200) {
      return response.data;
    } else {
     console.log(`error get movie by id : ${response.data}`)
    }
  } catch (error) {
    console.error('Error get movie by id:', error);
  }
}

export const updateMovieById = async (movie: Movie) => {
  try {
    const response = await axios.put(`${BASE_URL}${MOVIE_BASE_URL}/${movie.Id}`, {title: movie.Title, description: movie.Description});
    if (response.status === 200) {

      router.push('/');
  } else {
     console.log(`error update movie by id : ${response.data}`)
    }
  } catch (error) {
    console.error('Error update movie by id:', error);
  }
}

export const deleteMovieById = async (id: number) => {
  try {
    const response = await axios.delete(`${BASE_URL}${MOVIE_BASE_URL}/${id}`);
    if (response.status === 200) {

      router.push('/');
  } else {
     console.log(`error delete movie by id : ${response.data}`)
    }
  } catch (error) {
    console.error('Error delete movie by id:', error);
  }
}
