const { faker } = require("@faker-js/faker");
const Movie = require("../models/movieModel");

let seedId = 0;

function createRandomMovie() {
  return new Movie(
    seedId,
    faker.commerce.productName(),
    faker.commerce.productDescription()
  );
}

function generateMoviesArray(total) {
  const result = [];
  for (let i = 0; i < total; i++) {
    result.push(createRandomMovie());
    ++seedId;
  }
  return result;
}

module.exports = {
  generateMoviesArray,
};
