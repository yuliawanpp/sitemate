const express = require("express");
const MovieService = require("../services/movieService");
const Movie = require("../models/movieModel");
const { DEFAULT_PAGE, DEFAULT_PAGE_SIZE } = require("../config/constant");

const router = express.Router();

// get pagination movies
router.get("/movies", (req, res) => {
  try {
    const pageNumber = parseInt(req.query.page) || DEFAULT_PAGE;
    const pageSize = parseInt(req.query.pageSize) || DEFAULT_PAGE_SIZE;

    const movies = MovieService.getMovies({ pageNumber, pageSize });
    const totalMovies = MovieService.totalMovies();
    return res.status(200).json({
      data: movies,
      total: totalMovies,
      totalPages: Math.ceil(totalMovies / pageSize),
      currentPage: pageNumber,
      pageSize: pageSize,
    });
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
});

// GET /movies/:id route to fetch a specific movie by ID
router.get("/movies/:id", (req, res) => {
  try {
    const id = parseInt(req.params.id);

    const movie = MovieService.getMovieById(id);
    if (!movie) {
      return res.status(404).json({ error: `Movie with ID ${id} not found.` });
    }

    return res.status(200).json(movie);
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
});

// POST /movies route to add a new movie
router.post("/movies", (req, res) => {
  try {
    const { title, description } = req.body;

    const newMovie = MovieService.addMovie({ title, description });
    return res.status(201).json(newMovie);
  } catch (error) {
    res.status(400).json({ error: error.message });
  }
});

// PUT /movies/:id route to update an existing movie
router.put("/movies/:id", (req, res) => {
  try {
    const id = parseInt(req.params.id);
    const { title, description } = req.body;

    const updatedMovie = MovieService.updateMovieById(
      new Movie(id, title, description)
    );

    return res.status(200).json(updatedMovie);
  } catch (error) {
    res.status(400).json({ error: error.message });
  }
});

// DELETE /movies/:id route to delete a movie by ID
router.delete("/movies/:id", (req, res) => {
  try {
    const id = parseInt(req.params.id);

    MovieService.deleteMovie(id);
    return res.status(204).send();
  } catch (error) {
    res.status(400).json({ error: error.message });
  }
});

module.exports = router;
