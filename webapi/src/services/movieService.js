const seeder = require("../data/movieSeed");
const Movie = require("../models/movieModel");
const { DEFAULT_PAGE, DEFAULT_PAGE_SIZE } = require("../config/constant");

class MovieService {
  constructor() {
    // seed the movie array with 10 seeded data
    this.movies = [...seeder.generateMoviesArray(10)];
  }

  getMovies({ pageNumber, pageSize }) {
    if (typeof pageNumber !== "number" || typeof pageSize !== "number") {
      throw new Error("pageNumber and pageSize must be numbers");
    }

    let cleanPageNumber = pageNumber <= 0 ? DEFAULT_PAGE : pageNumber;
    const cleanPageSize = pageSize <= 0 ? DEFAULT_PAGE_SIZE : pageSize;
    const startIndex = (cleanPageNumber - 1) * pageSize;
    const endIndex = startIndex + cleanPageSize;
    return this.movies.slice(startIndex, endIndex);
  }

  getMovieById(id) {
    return this.movies[id];
  }

  totalMovies() {
    return this.movies.length;
  }

  updateMovieById(movieInput) {
    if (!(movieInput instanceof Movie)) {
      throw new Error(`Update Movie need the correct format`);
    }

    const movie = this.getMovieById(movieInput.Id);

    if (!movie) {
      throw new Error(`Movie with ID ${movieInput.Id} not found.`);
    }

    if (movieInput.Title) {
      movie.Title = movieInput.Title;
    }

    if (movieInput.Description) {
      movie.Description = movieInput.Description;
    }

    return movie;
  }

  addMovie({ title, description }) {
    const id = this.movies.length;

    // Check title and description not null
    if (
      typeof title !== "string" ||
      title.length <= 0 ||
      typeof description !== "string"
    ) {
      throw new Error("Title and Description have to be string");
    }
    const newMovie = new Movie(id, title, description);
    this.movies.push(newMovie);
    return newMovie;
  }

  deleteMovie(id) {
    const movie = this.getMovieById(id);

    if (!movie) {
      throw new Error(`Movie with ID ${id} not found.`);
    }

    // Remove movie from the array
    const deletedMovie = this.movies.splice(id, 1)[0];
    return deletedMovie;
  }
}

module.exports = new MovieService();
