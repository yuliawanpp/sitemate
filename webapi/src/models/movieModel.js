class Movie {
  constructor(id, title, description) {
    this.Id = id;
    this.Title = title;
    this.Description = description;
  }
}

module.exports = Movie;
