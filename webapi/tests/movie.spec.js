const request = require("supertest");
const app = require("../app");

describe("Movies API", () => {
  // Get
  it("GET /api/movies should return 200 OK when Query is empty", async () => {
    const response = await request(app).get("/api/movies");
    expect(response.status).toBe(200);
    expect(Array.isArray(response.body?.data)).toBe(true);
    expect(typeof response.body?.total).toBe("number");
    expect(typeof response.body?.totalPages).toBe("number");
    expect(typeof response.body?.currentPage).toBe("number");
    expect(typeof response.body?.pageSize).toBe("number");
  });

  it("GET /api/movies should return 200 OK and return data as Total PageSize", async () => {
    const response = await request(app)
      .get("/api/movies")
      .query({ page: 1, pageSize: 2 });
    expect(response.status).toBe(200);
    expect(Array.isArray(response.body?.data)).toBe(true);
    expect(response.body.data.length).toBe(2);
    expect(typeof response.body?.total).toBe("number");
    expect(typeof response.body?.totalPages).toBe("number");
    expect(typeof response.body?.currentPage).toBe("number");
    expect(typeof response.body?.pageSize).toBe("number");
  });

  it("GET /api/movies should return 200 OK and return data when Query is number", async () => {
    const response = await request(app)
      .get("/api/movies")
      .query({ page: -1, pageSize: -2 });
    expect(response.status).toBe(200);
    expect(Array.isArray(response.body?.data)).toBe(true);
    expect(typeof response.body?.total).toBe("number");
    expect(typeof response.body?.totalPages).toBe("number");
    expect(typeof response.body?.currentPage).toBe("number");
    expect(typeof response.body?.pageSize).toBe("number");
  });

  // Assuming seeder for movie is working
  it("GET /api/movies/:id should return Data for existing movie", async () => {
    const response = await request(app).get("/api/movies/0");
    expect(response.status).toBe(200);
    console.log(response.body);
    expect(response.body?.Id).toBe(0);
    expect(typeof response.body?.Title).toBe("string");
    expect(typeof response.body?.Description).toBe("string");
  });

  // Assuming seeder for movie only has 10 items
  it("GET /api/movies/:id should return 404 Not Found for non-existent movie", async () => {
    const response = await request(app).get("/api/movies/999");
    expect(response.status).toBe(404);
  });

  // Create
  it("should create a new movie", async () => {
    const newMovieData = {
      title: "New Movie",
      description: "Description of the new movie",
    };

    const response = await request(app).post("/api/movies").send(newMovieData);

    expect(response.status).toBe(201);
    expect(response.body.Title).toBe(newMovieData.title);
    expect(response.body.Description).toBe(newMovieData.description);

    expect(response.body.Id).toBeDefined();
  });

  it("should return 400 Bad Request if title is missing", async () => {
    const invalidMovieData = {
      description: "New Movie Without Title",
    };

    const response = await request(app)
      .post("/api/movies")
      .send(invalidMovieData);

    expect(response.status).toBe(400);
    expect(response.body.error).toBe("Title and Description have to be string");
  });

  it("should return 400 Bad Request if description is missing", async () => {
    const invalidMovieData = {
      title: "New Movie Without Description",
    };

    const response = await request(app)
      .post("/api/movies")
      .send(invalidMovieData);

    expect(response.status).toBe(400);
    expect(response.body.error).toBe("Title and Description have to be string");
  });

  // Update
  // Update movie id 0
  it("should update the movie with given ID", async () => {
    const movieId = 0;
    const updatedMovieData = {
      id: movieId,
      title: "Updated Movie Title",
      description: "Updated Movie Description",
    };

    const response = await request(app)
      .put(`/api/movies/${movieId}`)
      .send(updatedMovieData);

    expect(response.status).toBe(200);
    expect(response.body.Title).toBe(updatedMovieData.title);
    expect(response.body.Description).toBe(updatedMovieData.description);
  });

  // Update movie 999 which not exist
  it("should return 404 Not Found for non-existent movie", async () => {
    const movieId = 999;
    const updatedMovieData = {
      id: movieId,
      title: "Updated Movie Title",
      description: "Updated Movie Description",
    };

    const response = await request(app)
      .put(`/api/movies/${movieId}`)
      .send(updatedMovieData);

    expect(response.status).toBe(400);
  });

  // Delete
  // Assuming movie with Id 1 exists
  it("should delete the movie with given ID", async () => {
    const movieId = 1;

    const response = await request(app).delete(`/api/movies/${movieId}`);

    expect(response.status).toBe(204);
  });

  it("should return 404 Not Found for non-existent movie", async () => {
    const nonExistentMovieId = 999; // Assuming movie with ID 999 does not exist

    const response = await request(app).delete(
      `/api/movies/${nonExistentMovieId}`
    );

    expect(response.status).toBe(400);
  });
});
