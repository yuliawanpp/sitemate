const express = require("express");
const dotenv = require("dotenv");
const cors = require("cors");
const movieController = require("./src/controllers/movieController");

dotenv.config();

const app = express();
app.use(express.json());
app.use(cors());

const PORT = process.env.PORT || 3001;

app.use("/api", movieController);

// 404 Middleware
app.use((req, res) => {
  res.status(404).json({ error: "Not Found" });
  logger.warn(`404 Not Found: ${req.method} ${req.url}`);
});

// Central Error Handling Middleware
app.use((err, req, res, next) => {
  logger.error(`500 Internal Server Error: ${err.message}`);
  res.status(500).json({ error: "Internal Server Error" });
});

// Start server
app.listen(PORT, () => {
  console.log(`Server is running at http://localhost:${PORT}`);
});

module.exports = app;
